minetest mod floatland
============================

Simple mod to added rare greenbiomes to floadlands event only snown

Information
-----------

This mod is named `floatland`, it produces a large floadlands, 
but the biome is more nice event simple snown when others mods like ethereal 
are present.

Technical info
--------------

This is a fork for minenux project, from original work of floadlands_realm 
its just to provide nice biomes when other mods like ethereal only produces snown.

it uses the setting from minetest config files of mapgen v7, specially the `mgv7_floatland_level` 
and try to redefined the floatland layer of biome fully.

#### Status

Currently only some nodes are defined, still pending to define digging of tools.

#### Depends

* mgv7 mapgen only
* default mod

#### Blocks

| Node/item       | tecnical names         | related/drop |  note                    |
| --------------- | ---------------------- | ------------ | ------------------------ |
| High Grass      | floatland:grass  | default:dirt | top block, use diamond shovel |
| High Dirt       | floatland:dirt   |              | |
| High Stone      | floatland:stone  | floatland:dirt | |
| High Sand       | floatland:sand   |              | top block, use diamond shovel |
| High Rare Sand  | floatland:rare_sand |           | |
| High Crystal Sand | floatland:sand_crystal_block | floatland:sand_crystals 6 | |
| High Sand Crystal | floatland:sand_crystals | | |
| High Salt       | floatland:salt   |              | |

#### Biomes

| Name            | technical names  | nodes of biome |
| --------------- | ---------------- | ---------------------------------------------- |
| Floatland Beach | floatland_nicebeach | floatland:grass, floatland:dirt, floatland:stone |
| Floatland grass | floatland_nicegrass | floatland:sand, floatland:stone |


# License

CC BY SA 3 mckayshirou

Based in Amaz an archived work of it are at https://github.com/Amaz1/floatland_realm
Artwork of `sand_crystal_<>` based on runsy saltd mod https://github.com/runsy/saltd
The noise code is based on the floatlands C++ noise code. 
See https://github.com/minetest/minetest/blob/28841961ba91b943b7478704181604fa3e24e81e/src/mapgen_v7.cpp#L415

